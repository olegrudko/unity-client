using Assets.Scripts.Game.buttons;
using Assets.Scripts.Protocols;
using Assets.Scripts.Core;
using Assets.Scripts.States;
using Colyseus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private GameButton[] gameButtons;
    private Room<CarsRoomState> room;
    private int current = 0;
    private bool isStarted = false;

    public Room<CarsRoomState> Room { set { this.room = value; } }

    [SerializeField]
    GameButtonsController gameButtonsController;

    void Start() {}

    public void StartGame()
    {
        this.current = 0;
        this.isStarted = true;
        ActivateButton();
    }

    public bool Enabled
    {
        set
        {
            gameButtonsController.Enabled = value;
        }
    }

    private void ActivateButton()
    {
        switch (gameButtons[current].order)
        {
            case 0:
                gameButtonsController.ActivateButton0();
                break;
            case 1:
                gameButtonsController.ActivateButton1();
                break;
        }
    }

    private void DeactivateButton()
    {
        switch (gameButtons[current].order)
        {
            case 0:
                gameButtonsController.DeactivateButton0();
                break;
            case 1:
                gameButtonsController.DeactivateButton1();
                break;
        }
    }

    public GameButton[] GameButtons
    {
        set { this.gameButtons = value; }
    }

    // Send number of pressed button to the server
    public async void SendPressedButton(int order) {
        if (isStarted == false) { return; }

        DeactivateButton();
        PressControllerButtonProtocol pressButtonProtocol = new PressControllerButtonProtocol(order);
        await room.Send(ActionEvent.PressControllerButton, pressButtonProtocol);
        checkPressedButton(order);
        current++;
        ActivateButton();
    }

    private void checkPressedButton(int order) {
        if (gameButtons[current].order == order)
        {
            successPressed();
        } else
        {
            wrongPressed();
        }
    }

    private void successPressed()
    {

    }

    private void wrongPressed()
    {

    }
}
