using System;
using UnityEngine;

public class GameContext : MonoBehaviour
{
    private GameToken gameToken;
    
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public void Init(GameToken gameToken)
    {
        if (gameToken == null)
        {
            throw new Exception("Context init: game token should be defined");
        }
        this.gameToken = gameToken;
    }

    public int UserId
    {
        get { return gameToken.userId; }
    }
    
    public int PlayerId
    {
        get { return gameToken.playerId; }
    }
    
    public string Nickname
    {
        get { return gameToken.nickname; }
    }
    
    public string Username
    {
        get { return gameToken.username; }
    }
    
    public string Token
    {
        get { return gameToken.token; }
    }
}
