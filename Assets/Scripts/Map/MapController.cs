using Assets.Scripts.Protocols;
using Assets.Scripts.Shared;
using System.Collections.Generic;
using UnityEngine;
using Color = Assets.Scripts.Shared.Color;

public class MapController : MonoBehaviour
{
    [SerializeField]
    ServerEventProducer serverEventProducer;

    [SerializeField]
    GameEventProducer gameEventProducer;

    [SerializeField]
    PlayerOnMap playerPrefab;

    float width;
    float speedScale;
    string playerId = "";
    RectTransform rectTransform;

    Dictionary<string, PlayerOnMap> playersOnMap = new Dictionary<string, PlayerOnMap>();

    private void Start()
    {
        rectTransform = this.GetComponent<RectTransform>();
        width = rectTransform.rect.width;
        serverEventProducer.Subscribe<PrepareStartGameProtocol>(prepareGameEvent);
        serverEventProducer.Subscribe<PlayerInitialProtocol>(playerInitialEvent);
        serverEventProducer.Subscribe<GameObjectProtocol>(SpawnGameObject);
    }

    private void prepareGameEvent(PrepareStartGameProtocol data)
    {
        speedScale = width / data.mapDistance;
    }

    private void playerInitialEvent(PlayerInitialProtocol data)
    {
        playerId = data.id;
        foreach (GameObjectProtocol gameObject in data.gameObjects)
        {
            switch (gameObject.type)
            {
                case GameObjectType.Player:
                    createPlayerOnMap(gameObject.id, Color.White);
                    break;
            }
        }
    }

    private void createPlayerOnMap(string id, Color color)
    {
        PlayerOnMap playerOnMap = Instantiate(playerPrefab, new Vector3(0, 0), Quaternion.identity, transform);
        playersOnMap.Add(id, playerOnMap);
        playerOnMap.SetColor(color);

    }

    public void SetPlayerPositionOnMap(string playerId, Vector3 playerPosition)
    {
        if (playersOnMap.TryGetValue(playerId, out PlayerOnMap playerOnMap))
        {
            RectTransform rectTransform = playerOnMap.GetComponent<RectTransform>();
            rectTransform.anchoredPosition = new Vector3(playerPosition.x, 0) * speedScale;
        }
    }

    private void SpawnGameObject(GameObjectProtocol gameObjectData)
    {
        switch (gameObjectData.type)
        {
            case GameObjectType.Player:
                if (gameObjectData.id == playerId)
                {
                    createPlayerOnMap(gameObjectData.id, Color.Green);
                }
                else
                {
                    createPlayerOnMap(gameObjectData.id, Color.White);
                }
                break;
        }
    }
}
