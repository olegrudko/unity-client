using Assets.Scripts.Shared;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class PlayerOnMap : MonoBehaviour
{
    Image image;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetColor(Assets.Scripts.Shared.Color color)
    {
        image = GetComponent<Image>();
        switch (color)
        {
            case (Assets.Scripts.Shared.Color.Green):
                image.color = UnityEngine.Color.green;
                break;
            case (Assets.Scripts.Shared.Color.White):
                image.color = UnityEngine.Color.white;
                break;
            default:
                image.color = UnityEngine.Color.white;
                break;
        }
    }
}
