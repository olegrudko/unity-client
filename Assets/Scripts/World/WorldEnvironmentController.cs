using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldEnvironmentController : MonoBehaviour
{
    [SerializeField]
    int buildingWidth;

    [SerializeField]
    PlatformController player;

    [SerializeField] PrefabStorage prefabStorage;

    List<GameObject> buildingPrefabs;

    GameObject roadPrefab;

    [SerializeField]
    Vector3 cityOffset = new Vector3(0, 0, -50);

    Shared.Queue<GameObject> buildings;

    // Start is called before the first frame update
    void Start()
    {
        buildingPrefabs = prefabStorage.buildings;
        roadPrefab = prefabStorage.road;
        buildings = new Shared.Queue<GameObject>();
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            return;
        }

        while (buildings.Peek().transform.position.x < player.transform.position.x - buildingWidth * 2)
        {
            DestroyLeftBuilding();
        }

        while (buildings.Last.transform.position.x < player.transform.position.x + buildingWidth * 2)
        {
            CreateRightBuilding();
        }
    }

    public void TargetPlayer(PlatformController player)
    {
        this.player = player;
    }

    public void Initialize()
    {
        while(DestroyLeftBuilding()) {}
        for (int i = 0; i < 5; i++)
        {
            CreateRightBuilding();
        }
    }
    
    public bool DestroyLeftBuilding()
    {
        GameObject building;
        try
        {
            building = buildings.Dequeue();
        } catch
        {
            return false;
        }
        Destroy(building);
        return true;
    }

    public void CreateRightBuilding()
    {
        GameObject last = buildings.Last;
        GameObject randomPrefab1 = GetRandomBuildingPrefab();
        GameObject randomPrefab2 = GetRandomBuildingPrefab();
        GameObject road = Instantiate(roadPrefab);
        GameObject building1 = Instantiate(randomPrefab1);
        GameObject building2 = Instantiate(randomPrefab2);
        Vector3 position = building1.transform.position;
        Vector3 roadPosition = road.transform.position;
        
        if (last)
        {
            building1.transform.position = new Vector3(last.transform.position.x + buildingWidth + cityOffset.x, position.y + cityOffset.y, position.z + cityOffset.z);
            building2.transform.position = new Vector3(last.transform.position.x + buildingWidth * 1.5f + cityOffset.x, position.y + cityOffset.y, position.z + buildingWidth + cityOffset.z);
            road.transform.position = new Vector3(last.transform.position.x + buildingWidth + cityOffset.x, roadPosition.y + cityOffset.y, roadPosition.z - buildingWidth + cityOffset.z);
        } else
        {
            building1.transform.position = new Vector3(-buildingWidth * 2 + cityOffset.x, position.y + cityOffset.y, position.z + cityOffset.z);
            building2.transform.position = new Vector3(-buildingWidth * 1.5f + cityOffset.x, position.y + cityOffset.y, position.z + buildingWidth + cityOffset.z);
            road.transform.position = new Vector3(-buildingWidth * 1.5f + cityOffset.x, roadPosition.y + cityOffset.y, roadPosition.z - buildingWidth + cityOffset.z);
        }
        buildings.Enqueue(road);
        buildings.Enqueue(building2);
        buildings.Enqueue(building1);
    }

    private GameObject GetRandomBuildingPrefab()
    {
        int index = (int)UnityEngine.Random.Range(0, buildingPrefabs.Count - 0.0000001f);
        return buildingPrefabs[index];
    }
}
