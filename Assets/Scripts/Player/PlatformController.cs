using System;
using UnityEngine;
using Colyseus;
using UnityEngine.UI;
using Assets.Scripts.Protocols;
using Assets.Scripts.Shared;
using Assets.Scripts.States;

public class PlatformController : ServerObject
{
    private Vector3 speed = new Vector3(0, 0);
    public Vector3 realPosition = new Vector3();
    public Room<CarsRoomState> room;
    public string sessionId;
    public bool isPlayer = false;

    [SerializeField]
    public MapController mapController;

    //private Text debugText;

    // Start is called before the first frame update
    private async void Start()
    {
        //debugText = GameObject.Find("Debug").GetComponent<Text>();
    }

    // Update is called once per frame
    private async void Update()
    {
        realPosition += speed * Time.deltaTime;
        float distanceDelta = transform.position.x - realPosition.x;
        float speedCoefficient = distanceDelta != 0 ? Math.Abs(distanceDelta / speed.x) : 1;
        float slow = 5;
        if (distanceDelta < -speed.x)
        {
            speedCoefficient = 1 + (speedCoefficient - 1) / slow;
        } else if (distanceDelta > speed.x)
        {
            speedCoefficient = 1 + (1 - speedCoefficient) / slow;
        }

        speedCoefficient *= 1.1f;

        transform.Translate(new Vector3(speed.x * speedCoefficient * Time.deltaTime, 0, 0));
        
        if (mapController != null) {
            mapController.SetPlayerPositionOnMap(sessionId, transform.position);
        }
    }

    public void Move(MoveProtocol moveData)
    {
        long elapsedTime = (long) NetworkController.ElapsedTime;
        // Debug.Log($"Action time: {moveData.actionTime}; Client time: {elapsedTime}; diff: {moveData.actionTime - elapsedTime}");
        float elapsedEventTime = (moveData.actionTime - elapsedTime) / 1000f;
        realPosition = new Vector3(moveData.distance + moveData.speed * elapsedEventTime, realPosition.y, realPosition.z);
        speed.x = moveData.speed;
        Debug.Log(speed.x);
    }

    public void OnServerCollision(CollisionProtocol collisionData)
    {
        transform.position = new Vector3(collisionData.stopX, collisionData.stopY);
        //debugText.text += $"\n{this.sessionId} collided and moved to {collisionData.stopX}, {collisionData.stopY}";
    }
}
