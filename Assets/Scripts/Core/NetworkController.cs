using Assets.Scripts.Player;
using Assets.Scripts.Protocols;
using Assets.Scripts.Core;
using Assets.Scripts.Shared;
using Assets.Scripts.States;
using Colyseus;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class NetworkController : MonoBehaviour
{
    private static float elapsedTime;
    private GameContext gameContext;

    public static float ElapsedTime
    {
        get => elapsedTime;
        set => elapsedTime = value;
    }

    private Client client;
    private Room<CarsRoomState> room;
    
    [SerializeField] PrefabStorage prefabStorage;

    [SerializeField] StartGameTimerController startGameTimer;

    [SerializeField] GameController gameController;

    [SerializeField] ServerEventProducer eventProducer;

    [SerializeField] MapController mapController;

    [SerializeField] WorldEnvironmentController worldEnvironmentController;

    [SerializeField] CameraController cameraController;

    [SerializeField] private FinishUIController finishUIController;

    //private Dictionary<string, PlatformController> players;
    private Dictionary<string, ServerObject> gameObjects;

    //private Text debugText;

    // Start is called before the first frame update
    async void Start()
    {
        gameContext = GameObject.Find("GameContext").GetComponent<GameContext>();
        //players = new Dictionary<string, PlatformController>();
        gameObjects = new Dictionary<string, ServerObject>();

        //debugText = GameObject.Find("Debug").GetComponent<Text>();
        //debugText.text += "\nNetwork controller is running";

        //client = new Client("ws://olympic-321018.appspot.com/"); // cloud server
        client = new Client("ws://localhost:2567/"); // local

        await joinGameRoom(client);
        defineActions();
        subscribeEvents();
    }

    async private Task joinGameRoom(Client client)
    {
        Dictionary<string, object> initialData = new Dictionary<string, object>();
        initialData.Add("accessToken", gameContext.Token);
        room = await client.JoinOrCreate<CarsRoomState>("platform", initialData);
        gameController.Room = room;
        Debug.Log("Joined room successfully.");
    }

    private void defineActions()
    {
        defineAction<PlayerInitialProtocol>(ActionEvent.Initial);
        defineAction<MoveProtocol>(ActionEvent.Move);
        defineAction<CollisionProtocol>("collision");
        defineAction<GameObjectProtocol>(ActionEvent.SpawnGameObject);
        defineAction<PrepareStartGameProtocol>(ActionEvent.PrepareStartGame);
        defineAction<WinProtocol>(ActionEvent.Win);
    }

    private void defineAction<T>(string actionName)
    {
        //eventProducer.DefineProducer<T>();
        room.OnMessage<T>(actionName, eventProducer.Push<T>);
    }

    private void subscribeEvents()
    {
        eventProducer.Subscribe<PlayerInitialProtocol>(InitialPlayer);
        eventProducer.Subscribe<MoveProtocol>(MovePlayer);
        eventProducer.Subscribe<CollisionProtocol>(OnCollision);
        eventProducer.Subscribe<GameObjectProtocol>(SpawnGameObject);
        eventProducer.Subscribe<PrepareStartGameProtocol>(PrepareStartGame);
        eventProducer.Subscribe<WinProtocol>(Win);
    }

    private PlatformController CreatePlayer(string key, float x, float y, float z)
    {
        PlatformController player = Instantiate(prefabStorage.car, new Vector3(x, y, z), Quaternion.identity);
        player.realPosition = new Vector3(x, y, z);
        player.mapController = mapController;
        player.room = room;
        player.sessionId = key;
        gameObjects[key] = player;
        return player;
    }

    private void RemovePlayer(string key)
    {
        PlatformController player = (PlatformController) gameObjects[key];
        GameObject.Destroy(player.gameObject);
        gameObjects.Remove(key);
    }

    private void MovePlayer(MoveProtocol moveData)
    {
        PlatformController player = (PlatformController) this.gameObjects[moveData.sessionId];
        player.Move(moveData);
        //debugText.text += $"\n player {moveData.sessionId} start move!";
    }

    private void OnCollision(CollisionProtocol collisionData)
    {
        string sessionId = collisionData.objectId;
        PlatformController player = (PlatformController) this.gameObjects[sessionId];
        player.OnServerCollision(collisionData);
        //debugText.text += $"\n somewhere was collision";
    }

    private void InitialPlayer(PlayerInitialProtocol initialData)
    {
        foreach (GameObjectProtocol gameObject in initialData.gameObjects)
        {
            if (!gameObjects.TryGetValue(gameObject.id, out _))
            {
                switch (gameObject.type)
                {
                    case GameObjectType.Player:
                        CreatePlayer(gameObject.id, gameObject.x, gameObject.y, gameObject.z);
                        break;
                }
            }
        }

        //PlatformController player = (PlatformController)this.gameObjects[initialData.id];
        //player.isPlayer = true;
        elapsedTime = initialData.actionTime;
    }

    private void SpawnGameObject(GameObjectProtocol gameObjectData)
    {
        if (!gameObjects.TryGetValue(gameObjectData.id, out _))
        {
            switch (gameObjectData.type)
            {
                case GameObjectType.Player:
                    PlatformController player = CreatePlayer(
                        gameObjectData.id,
                        gameObjectData.x,
                        gameObjectData.y,
                        gameObjectData.z
                    );
                    if (gameObjectData.id == room.SessionId)
                    {
                        player.isPlayer = true;
                        worldEnvironmentController.TargetPlayer(player);
                        cameraController.SetTarget(player.transform);
                    }

                    break;
            }
        }
    }

    private void PrepareStartGame(PrepareStartGameProtocol data)
    {
        startGameTimer.SetCountdown(data.seconds);
        gameController.GameButtons = data.gameButtons;
    }

    private void Win(WinProtocol data)
    {
        Debug.Log($"Player: {data.sessionId} has won on position: {data.winPlace}");
        if (data.sessionId == room.SessionId)
        {
            gameController.Enabled = false;
            finishUIController.Appear(data.winPlace);
        }
    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime * 1000;
    }
    
    void OnApplicationQuit()
    {
        Debug.Log("Application ending after " + Time.time + " seconds");
    }
}