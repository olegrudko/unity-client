using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabStorage : MonoBehaviour
{
    public readonly List<GameObject> buildings = new List<GameObject>();
    public GameObject road { get; private set; }
    public PlatformController car { get; private set; }

    public Sprite pedalLeftActive;
    public Sprite pedalLeftInactive;
    public Sprite pedalRightActive;
    public Sprite pedalRightInactive;

    public Sprite finishBackground;

    // Start is called before the first frame update
    void Start()
    {
        buildings.Add(Resources.Load<GameObject>("Models/City1/city1"));
        buildings.Add(Resources.Load<GameObject>("Models/City2/city2"));
        buildings.Add(Resources.Load<GameObject>("Models/City3/city3"));
        road = Resources.Load<GameObject>("Models/Road/road");
        car = Resources.Load<PlatformController>("Models/Car/car");
        pedalLeftInactive = Resources.Load<Sprite>("UI/MoveButtons/pedal-left");
        pedalLeftActive = Resources.Load<Sprite>("UI/MoveButtons/pedal-left-light");
        pedalRightInactive = Resources.Load<Sprite>("UI/MoveButtons/pedal-right");
        pedalRightActive = Resources.Load<Sprite>("UI/MoveButtons/pedal-right-light");
        finishBackground = Resources.Load<Sprite>("UI/Finish/WinBackground");
    }
}
