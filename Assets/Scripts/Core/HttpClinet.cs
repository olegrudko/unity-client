using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Core
{
    public class PlatforceHttpClinet
    {
        HttpClient httpClient = new HttpClient();
        
        /// <summary>
        /// This method is used to begin sending request process.
        /// </summary>
        /// <param name="url">API url.</param>
        /// <param name="method">Request http/s method: Get, Post e.t.c.</param>
        /// <param name="body">Request body data.</param>
        /// <typeparam name="TRequestBody">Request body Type.</typeparam>
        /// <typeparam name="TResponseBody">Response body Type.</typeparam>
        public async Task<TResponseBody> SendRequest<TRequestBody, TResponseBody>(
            string url,
            HttpMethod method,
            TRequestBody body = null) where TRequestBody: class
        {
            StringContent content = GetContent(body);
            
            HttpResponseMessage response = await ProcessRequest(url, method, content);
            return ProcessResponse<TResponseBody>(response);
        }
        
        protected StringContent GetContent<T>(T body) {
            if (body != null)
            {
                string jsonBody = JsonUtility.ToJson(body);
                return new StringContent(jsonBody, Encoding.UTF8, "application/json");
            }

            return null;
        }

        protected async Task<HttpResponseMessage> ProcessRequest(
            string url,
            HttpMethod method,
            StringContent content)
        {
            if (method == HttpMethod.Get)
            {
                return await httpClient.GetAsync(url);
            }
            else if (method == HttpMethod.Post)
            {
                return await httpClient.PostAsync(url, content);
            }

            throw new Exception($"Unsupported http method: {method}");
        }

        protected TResponseBody ProcessResponse<TResponseBody>(HttpResponseMessage response)
        {
            if (response == null)
            {
                throw new Exception("Seems like connection error");
            }

            if (!response.IsSuccessStatusCode)
            {
                Debug.Log($"HttpError: {(int)response.StatusCode} {response.StatusCode}");
                throw new Exception($"{response.StatusCode}");
            }
            string result = response.Content.ReadAsStringAsync().Result;
            Debug.Log(result);
            return JsonUtility.FromJson<TResponseBody>(result);
        }
    }
    
}