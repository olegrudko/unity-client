using Assets.Scripts.Core;
using Assets.Scripts.Protocols;
using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class ServerEventProducer : MonoBehaviour
{
    Dictionary<Type, object> producers = new Dictionary<Type, object>();

    public void DefineProducer<T>() 
    {
        Type producerType = typeof(T);
        if (producers.TryGetValue(producerType, out object producer))
        {
            Debug.Log("Producer type is already defined");
        }
        producers.Add(producerType, new Subject<T>());
    }

    public void Subscribe<T>(Action<T> observer)
    {
        Subject<T> producer = tryGetProducer<T>();
        if (producer == null) 
        {
            producers.Add(typeof(T), new Subject<T>());
        }
        producer = tryGetProducer<T>();
        producer.Subscribe(observer);
    }

    public void Push<T>(T data)
    {
        Subject<T> producer = tryGetProducer<T>();
        producer.OnNext(data);
    }

    private Subject<T> tryGetProducer<T>()
    {
        Type producerType = typeof(T);
        object producer;
        if (producers.TryGetValue(producerType, out producer))
        {
            return (Subject<T>)producer;
        }
        return null;
    }
}
