﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Core
{
    static class ActionEvent
    {
        public const string Initial = "player-initial";
        public const string Move = "move";
        public const string SpawnGameObject = "spawn-game-object";
        public const string PrepareStartGame = "prepare-start-game";
        public const string PressControllerButton = "press-controller-button";
        public const string Win = "win";
    }
}
