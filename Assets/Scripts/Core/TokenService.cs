using System;
using System.Collections.Generic;
using JWT;
using JWT.Algorithms;
using JWT.Exceptions;
using JWT.Serializers;
using UnityEngine;

namespace Assets.Scripts.Core
{
    public class TokenService
    {
        private IJsonSerializer serializer;
        IDateTimeProvider provider;
        IJwtValidator validator;
        IBase64UrlEncoder urlEncoder;
        private IJwtAlgorithm algorithm;
        IJwtDecoder decoder;

        public TokenService()
        {
            serializer = new JsonNetSerializer();
            provider = new UtcDateTimeProvider();
            validator = new JwtValidator(serializer, provider);
            urlEncoder = new JwtBase64UrlEncoder();
            algorithm = new HMACSHA256Algorithm(); // symmetric
            decoder = new JwtDecoder(serializer, validator, urlEncoder, algorithm);
        }
        
        public string DecodeToJson(string token)
        {
            string secret = "";
            try
            {
                var json = decoder.Decode(token, secret, verify: false);
                return json;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

            return "";
        }
        
        public Dictionary<string, object> DecodeToDictionary(string token)
        {
            string secret = "";
            try
            {
                var value = decoder.DecodeToObject<Dictionary<string, object>>(token, secret, verify: false);
                return value;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

            return null;
        }
    }
}