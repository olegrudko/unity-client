using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class GameEventProducer : MonoBehaviour
{
    Dictionary<string, object> producers = new Dictionary<string, object>();

    public void Subscribe<T>(string eventName, Action<T> observer)
    {
        Subject<T> producer = tryGetProducer<T>(eventName);
        if (producer == null)
        {
            producers.Add(eventName, new Subject<T>());
        }
        producer = tryGetProducer<T>(eventName);
        producer.Subscribe(observer);
    }

    public void Push<T>(string eventName, T data)
    {
        Subject<T> producer = tryGetProducer<T>(eventName);
        producer.OnNext(data);
    }

    private Subject<T> tryGetProducer<T>(string eventName)
    {
        object producer;
        if (producers.TryGetValue(eventName, out producer))
        {
            return (Subject<T>)producer;
        }
        return null;
    }
}
