using UnityEngine;
using UnityEngine.UI;
using System;
using System.Net.Http;
using Assets.Scripts.Core;
using UI.Dialog;
using UnityEngine.SceneManagement;
using Object = System.Object;

[Serializable]
public class LoginRequest
{
    public string username;
    public string password;
    public string gameCode = "platforce";
}

public class LoginResponse
{
    public string access_token;
}

public class Login : MonoBehaviour
{
    private string baseUrl = "http://localhost:3000";
    private Color invalidColor = new Color(0.9245283f, 0.5712887f, 0.5712887f);
    public GameObject username;
    public GameObject password;
    public GameObject loginButton;

    private string Username;
    private string Password;

    private PlatforceHttpClinet httpClient;
    private TokenService tokenService;
    private GameContext gameContext;

    // Start is called before the first frame update
    void Start()
    {
        gameContext = GameObject.Find("GameContext").GetComponent<GameContext>();
        httpClient = new PlatforceHttpClinet();
        tokenService = new TokenService();
        username = GameObject.Find("Username");
        password = GameObject.Find("Password");
        loginButton = GameObject.Find("LoginButton");
        loginButton.GetComponent<Button>().onClick
            .AddListener(OnClickLoginButton);
    }

    // Update is called once per frame
    void Update()
    {
        if (username.GetComponent<InputField>().isFocused)
        {
            username.GetComponent<InputField>().image.color = Color.white;
        }

        if (password.GetComponent<InputField>().isFocused)
        {
            password.GetComponent<InputField>().image.color = Color.white;
        }

        Username = username.GetComponent<InputField>().text;
        Password = password.GetComponent<InputField>().text;
    }

    public async void OnClickLoginButton()
    {
        bool valid = this.CheckLoginForm();
        if (!valid)
        {
            Debug.Log("Username or password required");
            return;
        }
        LoginRequest body = new LoginRequest() {username = Username, password = Password};
        LoginResponse result = null;
        try
        {
            result = await httpClient.SendRequest<LoginRequest, LoginResponse>(
                $"{this.baseUrl}/auth/login",
                HttpMethod.Post,
                body
            );
        }
        catch (Exception error)
        {
            DialogMessage.Instance
                .SetTitle("Login Error")
                .SetMessage(error.Message)
                .Show();
            return;
        }

        if (result.access_token != null)
        {
            Debug.Log("All OK");
            var tokenData = tokenService.DecodeToDictionary(result.access_token);
            GameToken gameToken = new GameToken();
            gameToken.token = result.access_token;
            gameToken.username = (string) tokenData["username"];
            gameToken.userId = (int)(long) tokenData["userId"];
            gameToken.playerId = (int)(long) tokenData["playerId"];
            gameToken.nickname = (string) tokenData["nickname"];
            Debug.Log(gameToken);
            gameContext.Init(gameToken);
            SceneManager.LoadScene("LobbyScene");
        }
    }

    private bool CheckLoginForm()
    {
        bool valid = true;
        if (string.IsNullOrEmpty(Username))
        {
            username.GetComponent<InputField>().image.color = invalidColor;
            valid = false;
        }

        if (string.IsNullOrEmpty(Password))
        {
            password.GetComponent<InputField>().image.color = invalidColor;
            valid = false;
        }

        return valid;
    }
}