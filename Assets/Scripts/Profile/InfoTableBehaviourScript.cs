using UnityEngine;
using UnityEngine.UI;
using System.Net.Http;
using Assets.Scripts.Core;
using System.Threading.Tasks;
using System.Text;
using Newtonsoft.Json;
using System;

public class ProfileRequest
{
    public string nickname;
}

public class ProfileResponse
{
    public string nickname;
    public float rating;
    public string playerId;
}

public class InfoTableBehaviourScript : MonoBehaviour
{
    private PlatforceHttpClinet httpClient;
    private TokenService tokenService;
    private GameContext gameContext;
    private string baseUrl = "http://localhost:3000";

    private ProfileResponse profile;

    private Text ratingValueComp;
    private InputField nameValueComp;
    private Button changeNameButtonComp;
    private bool isNameEditing = false;

    // Start is called before the first frame update
    void Start()
    {
        gameContext = GameObject.Find("GameContext").GetComponent<GameContext>();
        httpClient = new PlatforceHttpClinet();
        tokenService = new TokenService();
        
        changeNameButtonComp = GameObject.Find("ChangeNameButton").GetComponent<Button>();
        changeNameButtonComp.onClick.AddListener(ChangeNameOnClick);

        Task renderInfo = RenderInfo();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public async void ChangeNameOnClick()
    {
        if (isNameEditing)
        {
            string newName = nameValueComp.text;
            await ChangeNickname(newName);
            nameValueComp.readOnly = true;
            isNameEditing = false;
        }
        else
        {
            nameValueComp.readOnly = false;
            isNameEditing = true;
        }
        ToggleChangeNameButtonText(isNameEditing);
    }

    void ToggleChangeNameButtonText(bool isEditing)
    {
        string text = isEditing ? "Save" : "Change";
        Text buttonTextComp = changeNameButtonComp.GetComponentInChildren<Text>();
        buttonTextComp.text = text;
    }

    private async Task RenderInfo()
    {
        profile = await GetProfile();

        if (profile != null)
        {
            RenderNickname(profile.nickname);
            RenderRating(profile.rating);
        }
    }

    private async Task<ProfileResponse> GetProfile()
    {
        return await httpClient.SendRequest<ProfileRequest, ProfileResponse>(
            $"{this.baseUrl}/profile/{this.gameContext.PlayerId}",
            HttpMethod.Get
        );
    }

    private void RenderNickname(string name)
    {
        nameValueComp = GameObject.Find("NameValue").GetComponent<InputField>();
        nameValueComp.text = name;
    }

    private void RenderRating(float rating)
    {
        ratingValueComp = GameObject.Find("RatingValue").GetComponent<Text>();
        ratingValueComp.text = rating.ToString();
    }

    private async Task<string> ChangeNickname(string nickname)
    {
        // TODO: do not use default HttpClient when "PATCH" with headers is implemented in PlatforceHttpClinet
        HttpClient httpClient = new HttpClient();

        string uri = $"{this.baseUrl}/profile/{this.gameContext.PlayerId}/nickname";
        HttpMethod httpMethod = new HttpMethod("PATCH");

        ProfileRequest body = new ProfileRequest { nickname = nickname };
        string jsonBody = JsonUtility.ToJson(body);
        StringContent content = new StringContent(jsonBody, Encoding.UTF8, "application/json");

        HttpRequestMessage request = new HttpRequestMessage(httpMethod, uri)
        { Content = content };

        request.Headers.Add("x-access-token", this.gameContext.Token);

        HttpResponseMessage response = await httpClient.SendAsync(request);

        if (response == null)
        {
            throw new Exception("Seems like connection error");
        }

        if (!response.IsSuccessStatusCode)
        {
            Debug.Log($"HttpError: {(int)response.StatusCode} {response.StatusCode}");
        }
        return response.Content.ReadAsStringAsync().Result;
    }
}
