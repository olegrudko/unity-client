﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Shared
{
    static class GameObjectType
    {
        public const string Player = "player";
        public const string Block = "block";
    }
}
