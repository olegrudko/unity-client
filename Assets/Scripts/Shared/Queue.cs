﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    class Queue<T>: System.Collections.Generic.Queue<T>
    {
        public T Last { get; private set; }

        public void Enqueue(T item)
        {
            base.Enqueue(item);
            Last = item;
        }
    }
}
