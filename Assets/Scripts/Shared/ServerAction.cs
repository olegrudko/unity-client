﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Shared
{
    class ServerAction<TAction, TData>
    {
        public Type actionType = typeof(TAction);
        public Type dataType = typeof(TData);

        public TAction action;
        public TData data;

        public ServerAction(TAction action, TData data)
        {
            this.action = action;
            this.data = data;
        }
    }
}
