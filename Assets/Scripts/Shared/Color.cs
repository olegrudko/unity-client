﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Shared
{
    public enum Color
    {
        White,
        Black,
        Red,
        Green,
        Blue
    }
}
