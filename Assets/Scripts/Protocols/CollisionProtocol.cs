﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Protocols
{
    public class CollisionProtocol
    {
        public string objectId;
        public CollisionData[] collisions;
        public float stopX;
        public float stopY;
        public long actionTime;
    }

    public class CollisionData
    {
        public string id;
    }
}
