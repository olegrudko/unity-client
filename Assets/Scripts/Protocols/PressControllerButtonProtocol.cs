﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Protocols
{
    public class PressControllerButtonProtocol
    {
        public int order;

        public PressControllerButtonProtocol() { }

        public PressControllerButtonProtocol(int order)
        {
            this.order = order;
        }
    }
}
