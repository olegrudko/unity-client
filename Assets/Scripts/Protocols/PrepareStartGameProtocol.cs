﻿using Assets.Scripts.Game.buttons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Protocols
{
    public class PrepareStartGameProtocol: RealtimeProtocol
    {
        public int seconds;
        public int mapDistance;
        public GameButton[] gameButtons;

        public PrepareStartGameProtocol()
        {

        }
    }
}
