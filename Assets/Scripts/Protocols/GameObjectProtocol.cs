﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Protocols
{
    public class GameObjectProtocol
    {
        public string id;
        public string type;
        public float x;
        public float y;
        public float z;
        public long actionTime;

        public GameObjectProtocol() { }

        public GameObjectProtocol(string id, string type, float x, float y)
        {
            this.id = id;
            this.type = type;
            this.x = x;
            this.y = y;
        }
    }
}
