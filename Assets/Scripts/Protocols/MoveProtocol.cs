﻿namespace Assets.Scripts.Protocols
{
    public class MoveProtocol: RealtimeProtocol
    {
        public string sessionId;
        public float speed;
        public int distance;

        public MoveProtocol() { }

        public MoveProtocol(string sessionId, int speed, int distance)
        {
            this.sessionId = sessionId;
            this.speed = speed;
            this.distance = distance;
        }
    }
}
