﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Protocols
{
    public class PlayerInitialProtocol
    {
        public string id;
        public long actionTime;
        public GameObjectProtocol[] gameObjects;

        public PlayerInitialProtocol() {}
    }
}
