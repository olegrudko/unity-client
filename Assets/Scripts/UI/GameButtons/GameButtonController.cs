using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Player;
using UnityEngine;
using UnityEngine.UI;

public class GameButtonController : MonoBehaviour
{
    Button button;
    Image image;
    public ButtonType buttonType;
    private Sprite sprite;
    private Sprite activeSprite;
    public PrefabStorage prefabStorage;
    public int order;

    private void Start()
    {
        if (buttonType == ButtonType.Left)
        {
            sprite = prefabStorage.pedalLeftInactive;
            activeSprite = prefabStorage.pedalLeftActive;
        }
        else if (buttonType == ButtonType.Right)
        {
            sprite = prefabStorage.pedalRightInactive;
            activeSprite = prefabStorage.pedalRightActive;
        }
        button = GetComponent<Button>();
        image = GetComponent<Image>();
        image.sprite = sprite;
    }

    public bool Enabled
    {
        set
        {
            if (button != null)
            {
                button.enabled = value;
            }
            if (image != null)
            {
                image.enabled = value;
            }
        }
    }

    public void Activate()
    {
        image.sprite = activeSprite;
    }

    public void Deactivate()
    {
        image.sprite = sprite;
    }
}
