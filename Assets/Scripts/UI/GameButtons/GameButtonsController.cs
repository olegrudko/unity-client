using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameButtonsController : MonoBehaviour
{
    [SerializeField]
    GameButtonController gameButton0;
    [SerializeField]
    GameButtonController gameButton1;

    public bool Enabled
    {
        set
        {
            gameButton0.Enabled = value;
            gameButton1.Enabled = value;
        }
    }
    
    public void ActivateButton0()
    {
        gameButton0.Activate();
    }

    public void ActivateButton1()
    {
        gameButton1.Activate();
    }

    public void DeactivateButton0()
    {
        gameButton0.Deactivate();
    }

    public void DeactivateButton1()
    {
        gameButton1.Deactivate();
    }
}
