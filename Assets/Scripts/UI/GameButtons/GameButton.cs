﻿using Assets.Scripts.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Game.buttons
{
    public class GameButton
    {
        public int id;
        public Color color;
        public int order;
    }
}
