using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Dialog
{
    public class DialogMessage : MonoBehaviour
    {
        [SerializeField] private Text title;
        [SerializeField] private Text message;
        [SerializeField] private Button button;
        private Canvas canvas;

        private DialogMessageData dialogMessageData = new DialogMessageData();

        public static DialogMessage Instance = null;
    
        void Awake()
        {
            canvas = this.GetComponent<Canvas>();
            if (Instance != null)
            {
                Destroy(this);
                throw new Exception("DialogMessage already exists");
            }
            Instance = this;
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(Hide);
            
            Hide();
        }

        void Start()
        {
            DontDestroyOnLoad(this.gameObject);
        }

        public DialogMessage SetTitle(string title)
        {
            this.dialogMessageData.title = title;
            return Instance;
        }
        
        public DialogMessage SetMessage(string message)
        {
            this.dialogMessageData.message = message;
            return Instance;
        }

        public DialogMessage Show()
        {
            canvas.enabled = true;
            title.text = this.dialogMessageData.title;
            message.text = this.dialogMessageData.message;
            this.dialogMessageData = new DialogMessageData();
            return Instance;
        }

        public void Hide()
        {
            canvas.enabled = false;
        }
    }
}
