using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartGameTimerController : MonoBehaviour
{
    [SerializeField]
    private Text startGameText;

    [SerializeField]
    private GameController gameController;

    private int time;

    void Start()
    {
        startGameText.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SetCountdown(int time)
    {
        this.time = time;
        StartCoroutine(timer());
    }

    private IEnumerator timer()
    {
        startGameText.enabled = true;
        while (time > 0)
        {
            redrawTime();
            yield return new WaitForSeconds(1);
            time--;
        }
        gameController.StartGame();
        startGameText.enabled = false;
    }

    private void redrawTime()
    {
        startGameText.text = time.ToString();
    }
}
