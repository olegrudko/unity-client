using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FinishUIController : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI placeNumberUIText;
    
    [SerializeField]
    private PrefabStorage prefabStorage;
    
    [SerializeField]
    private Image image;
    
    void Start()
    {
        setVisible(false);
    }
    
    public void Disappear(int place)
    {
        placeNumberUIText.text = string.Empty;
        setVisible(false);
    }

    public void Appear(int place)
    {
        if (place < 1)
        {
            throw new Exception("Place must be more then 0");
        }

        placeNumberUIText.text = place.ToString();
        setVisible(true);
    }

    private void setVisible(bool visible)
    {
        Canvas canvas = GetComponent<Canvas>();
        image.sprite = prefabStorage.finishBackground;
        canvas.enabled = visible;
    }
}
